// See https://theme-ui.com/ for more info and also https://www.gatsbyjs.org/docs/theme-ui/

import { tailwind, baseColors } from "@theme-ui/preset-tailwind"
import prism from "@theme-ui/prism/presets/oceanic-next"
import "typeface-raleway"

export default {
  initialColorModeName: "light",
  useColorSchemeMediaQuery: true,
  ...tailwind,
  breakpoints: ["580px", "768px", "1200px", "1440px"],
  fonts: {
    ...tailwind.fonts,
    body: "Raleway, sans-serif",
    siteTitle: "Raleway, sans-serif",
    navLinks: "inherit",
    alt: "inherit",
  },
  colors: {
    ...tailwind.colors,

    primary: "#577018",
    //primary: baseColors.orange[7],
    primaryHover: baseColors.orange[8],
    secondary: baseColors.orange[6],
    accent: "#9ce5f4",
    textBlack: baseColors.gray[8],
    textWhite: baseColors.gray[1],
    codeBg: baseColors.gray[9],
    header: {
      background: "white",
      backgroundOpen: "#2e7da4",
      text: "#577018",
      textOpen: baseColors.gray[1],
      icons: "#E07931",
      iconsHover: "#59344F",
      iconsOpen: "#59344F",
    },

    main: {
      my: 0,
      color: "black", //ATTENTION: use until in lightMode!!!
    },

    footer: {
      background: "white",
      //text: baseColors.gray[8],
      text: "#577018",
      //links: baseColors.gray[8],
      links: "black",
      //icons: baseColors.gray[8],
      color: "#87A54F",
      icons: "#E07931",
      iconsHover: "#8EA34E", //moss green
    },
    modes: {
      dark: {
        text: baseColors.gray[1],
        //background: baseColors.gray[9],
        background: "black",
        //primary: "#da6327",
        primary: "#577018",
        secondary: baseColors.orange[6],
        accent: "#00616f",
        gray: baseColors.gray[5],
        muted: baseColors.gray[8],
        codeBg: baseColors.gray[8],
        header: {
          backgroundOpen: "#324b50",
          //text: baseColors.gray[1],
          text: "#577018", //vert olive
          textOpen: baseColors.gray[1],
          //icons: baseColors.gray[5],
          icons: "#E07931", //princetone orange
          iconsHover: "#8EA34E", //moss green
          iconsOpen: baseColors.gray[5],
          navLi: "#577018",
        },

        main: {
          my: 0,
          //color: "#56823A",
          color: "white", //ATTENTION: use until in darkMode!!!
        },
        gradients: {
          blue: {
            backgroundImage: t =>
              `linear-gradient(45deg, ${t.colors.orange[3]}, ${t.colors.orange[5]})`,
          },
        },
        footer: {
          background: "white",
          //text: baseColors.gray[1],
          text: "#577018", //vert olive
          icons: "#E07931", //princetone orange
          //icons: baseColors.gray[1],
          textAlign: "center",
          links: baseColors.gray[1],
        },
      },
    },
  },
  sizes: {
    ...tailwind.sizes,
    maxPageWidth: "1440px", // Sets the max width of elements like the header/footer on really large screens
    maxContentWidth: "720px", // Sets the container size on larger screens, e.g. tablets and laptops
    contentWidth: "90vw", // Sets the container width on smaller screens, results in a 5vw margin on the left and right
    headerHeight: "auto", // Provides fallback setting to control header height
    logoWidthXS: "61px", // Logo width on extra small screens, up to 480px
    logoWidthS: "76px", // Logo width on small screens, 480px - 768px
    logoWidthM: "76px", // Logo width on medium screens, 768px - 1024px
    logoWidthL: "92px", // Logo width on large screens, 1024px - 1440px
    logoWidthXL: "92px", // Logo width on extra large screens, above 1440px
    logoHeightXS: "40px", // Logo height on extra small screens, up to 480px
    logoHeightS: "50px", // Logo height on small screens, 480px - 768px
    logoHeightM: "50px", // Logo height on medium screens, 768px - 1024px
    logoHeightL: "60px", // Logo height on large screens, 1024px - 1440px
    logoHeightXL: "60px", // Logo height on extra large screens, above 1440px
    iconsFooter: "32px", // Sets the icons size for the footer
    iconsHeader: "20px", // Sets the icons size for the header
  },
  styles: {
    ...tailwind.styles,
    root: {
      backgroundColor: "background",
      color: "text",
      fontFamily: "body",
      fontWeight: "body",
      lineHeight: "body",
      fontSize: 2,
    },
    a: {
      //color: "primary",
      color: "#orange",
      textDecoration: "none",
      ":hover": {
        textDecoration: "underline",
      },
    },
    h1: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      m: 0,
      mb: 1,
      fontSize: 6,
      mt: 4,
    },
    h2: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      m: 0,
      mb: 1,
      fontSize: 5,
      mt: 4,
    },
    h3: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      m: 0,
      mb: 1,
      fontSize: 4,
      mt: 3,
    },
    h4: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      m: 0,
      mb: 1,
      fontSize: 3,
    },
    h5: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      m: 0,
      mb: 1,
      fontSize: 2,
    },
    h6: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      m: 0,
      mb: 2,
      fontSize: 1,
    },
    blockquote: {
      bg: "muted",
      p: 3,
      my: 4,
      mx: [1, 2, 4, null, null],
      borderLeft: "5px solid",
      borderColor: "primary",
    },
    inlineCode: {
      ...prism,
      fontSize: 1,
      p: 1,
      backgroundColor: "muted",
      color: "text",
    },
    pre: {
      ...prism,
      fontSize: 1,
      p: 3,
      my: 4,
      overflowX: "auto",
      backgroundColor: "codeBg",
      width: ["100vw", null, "100%", null, null],
      position: ["relative", null, "static", null, null],
      left: ["calc(-50vw + 50%)", null, "auto", null, null],
    },
  },
  images: {
    avatar: {
      width: 48,
      height: 48,
      borderRadius: 99999,
    },
  },
  buttons: {
    primary: {
      bg: "#bb5420",
      color: "textWhite",
      borderRadius: "default",
      transition: "all 0.3s ease 0s",
      "::after": {
        content: "\\00A0 \\2192",
      },
      ":hover": {
        bg: "primaryHover",
      },
    },
    secondary: {
      bg: "transparent",
      color: "primary",
      borderColor: "primary",
      borderStyle: "solid",
      borderWidth: "2px",
      borderRadius: "default",
      transition: "all 0.3s ease 0s",
      "::after": {
        content: "\\00A0 \\2192",
      },
      ":hover": {
        bg: baseColors.gray[1],
      },
    },
    alt: {
      bg: "transparent",
      border: "none",
      color: "primary",
      fontWeight: "bold",
      letterSpacing: "wider",
      px: 0,
      m: 0,
      transition: "all 0.3s ease 0s",
      "::after": {
        content: "\\00A0 \\2192",
      },
      ":hover": {
        textDecoration: "underline",
        bg: "transparent",
        border: "none",
      },
    },
  },
  variants: {
    siteHeader: {
      padding: "0 20px 0 20px",
    },
    header: {
      //border: "1px black solid",
      //borderRadius:"1px 5px 50px",
      //width:"200px",
      //height:"80px",
    },
    footer: {},
    footerLinks: {},

    siteTitle: {
      color: "#577018", //green olive
      //fontSize: [1, 15, null, 2, null],
      //fontSize: [1, 15, null, 4, null],
      fontSize: [1, 19, null, 3, null],
    },

    // eslint-disable-next-line no-dupe-keys
    footer: {
      textAlign: "center",
      color: "#577018",
    },

    postListContainer: {
      my: 5,
    },

    navUl: {
      color: "#B3433B",
    },

    navLi: {
      padding: "0 20px 0 20px",
      //width: "200px",
      height: "30px",
      color: "#B3433B",
      fontSize: [1, 15, null, 3, null],
      fontFamily: "Raleway, sans-serif",
    },

    navLink: {
      "::after": {
        position: "absolute",
        color: "#9C6341", //color grenerate by gardiant
        top: "100%",
        left: "0",
        width: "100%",
        height: "1px",
        backgroundColor: "primary",
        content: '""',
        opacity: "0",
        transition: "height 0.3s, opacity 0.3s, transform 0.3s",
        transform: "translateY(-6px)",
      },
      ":hover, :focus, :active": {
        textDecoration: "none",
        color: "#8EA34E",
      },
      ":hover::after, :active::after, :focus::after": {
        height: "2px",
        opacity: "1",
        transform: "translateY(0px)",
      },
    },
    navLinkActive: {
      textDecoration: "none",
      "::after": {
        position: "absolute",
        top: "100%",
        left: "0",
        width: "100%",
        height: "4px",
        backgroundColor: "primary",
        content: '""',
        opacity: "1",
        transform: "translateY(0px)",
      },
    },
    navLinkSub: {
      fontSize: [1, 15, null, 3, null],
      "::after": {
        content: "none",
      },
      ":hover::after": {
        content: "none",
      },
      ":hover, :focus, :active": {
        color: "primary",
      },
    },
    navLinkSubActive: {
      textDecoration: "none",
      color: "primary",
      fontSize: [1, 15, null, 3, null],
      "::after": {
        content: "none",
      },
    },
  },
}
