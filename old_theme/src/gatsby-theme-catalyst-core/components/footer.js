/* eslint-disable prettier/prettier */
/** @jsx jsx */
import { jsx, Link, Box } from "theme-ui"
import {
  useCatalystConfig,
} from "gatsby-theme-catalyst-core"

const SiteFooter = () => {
  //const { title } = useSiteMetadata()
  const { footerContentLocation } = useCatalystConfig()
  //const { theme } = useThemeUI()
  const isLeft = footerContentLocation === "left"
  const isRight = footerContentLocation === "right"
  const isCenter = footerContentLocation === "center"

  return (

<footer
      sx={{
        color: "footer.text",
        backgroundColor: "footer.background",
        textAlign:
          (isLeft && "left") || (isRight && "right") || (isCenter && "center"),
        px: 3,
        py: 3,
        gridArea: "footer",
        a: {
          color: "footer.links",
        },
        variant: "variants.footer",
      }}
    >
      <div
       sx={{
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        p: 2,
        variant: 'styles.footer',
      }}>
        <div
          sx={{
            a: {
              color: "footer.icons",
              mt: 30,
            },
            "a:last-of-type": {
              mr: 0,
            },
            "a:hover": {
              color: "primary",
            },
          }}
        >
          
        </div>
  <Box
    h1={12}
    mr={-6}
    ml={-6}
    m={0}
    mb={10}
    mt={30}
  >  
  <ul
     sx={{
      listStyle: 'none',
      //variant: 'animated.footerLink',
      //bgColor: '#f1f1f1',
      //width: '1000px',
      //margin: '1px',
      textAlign: 'left',
      //lineHeight: '75px',
      //fontSize: '30px',
      }}>
        <h3
        sx={{
          variant: 'footer.linkTitle',
        }}
      >Pourquoi Froggit ?
      </h3>
  
  <li>
      
    <Link to href='https://lydra.fr/es-2-ton-code-source-est-il-vraiment-en-securite/' sx={{ variant: 'footer.navlink', p: 2 }}> Ton code source est-il en sécurité ? </Link>
      
  </li>
  
  <li>
    
    <Link to href='https://lydra.fr/category/souverainete-numerique/' sx={{ variant: 'footer.navlink', p: 2 }}> La souveraineté numérique </Link>

  </li>
  
  </ul>

  </Box>

  <Box
    h1={12}
    mr={-6}
    ml={-6}
    m={0}
    mb={10}
    mt={30}
  >
  <ul
  sx={{
    listStyle: 'none',
    //variant: 'animated.footerLink',
    //bgColor: '#f1f1f1',
    //width: '1000px',
    //margin: '1px',
    //textAlign: 'right',
    //lineHeight: '75px',
    //fontSize: '30px',
    }}>        
<h3 sx={{
          variant:   'footer.linkTitle',
        }}>
    Communauté
  </h3>
          
  <li>
    <Link to href='/forum' sx={{ variant: 'footer.navlink', p: 2 }}>Forum</Link>
  </li>
    
  <li>
    <Link to href='/wiki' sx={{ variant: 'footer.navlink', p: 2 }}>Wiki</Link>
  </li> 
  
  </ul>
  </Box>
  

  <Box
    h1={12}
    mr={-6}
    ml={-6}
    m={0}
    mb={10}
    mt={188}
    
  >
  <ul
    sx={{
    listStyle: 'none',
    //variant: 'animated.footerLink',
    //bgColor: '#f1f1f1',
    //width: '1000px',
    //margin: '1px',
    //textAlign: 'right',
    //lineHeight: '75px',
    //fontSize: '30px',
    }}>        
          <h3 sx={{
          variant: 'footer.linkTitle',
        }}>
    Société
  </h3>
  
  <li>
    <Link to href='/a_propos' sx={{ variant: 'footer.navlink', p: 2 }}>A propos</Link>
    
  </li>

  <li>
   
    <Link to href='/contact' sx={{ variant: 'footer.navlink', p: 2 }}>Contact</Link>

  </li>

  <li>
    <Link to href='/equipe' sx={{ variant: 'footer.navlink', p: 2 }}>Equipe</Link>
  </li>
  
  <li>
    <Link to href='/mentions_legales' sx={{ variant: 'footer.navlink', p: 2 }}>Mentions légales</Link>
  </li>
  
  <li>
    <Link to href='https://oxalis-scop.fr/conditions-generales-de-vente' sx={{ variant: 'footer.navlink', p: 2 }}>CVG</Link>
  </li>

  <li>
    <Link to href='/cpv' sx={{ variant: 'footer.navlink', p: 2 }}>CPV</Link>
  </li>

  <li>
    <Link to href='/cgu' sx={{ variant: 'footer.navlink', p: 2 }}>CGU</Link>
  </li>

  </ul>
  
  </Box>
  </div>       
    </footer>
  )
}

export default SiteFooter
