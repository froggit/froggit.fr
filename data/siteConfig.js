module.exports = {
  siteTitle: "Froggit",
  siteDescription: "A Gatsby theme/starter to build lightning-fast websites",
  authorName: "Lydra",
  twitterUsername: "art_devops",
  authorAvatar: "avatar.jpeg", // file in content/images
  defaultLang: "fr", // show flag if lang is not default. Leave empty to enable flags in post lists
  authorDescription: `
  For the last decade, Maxence Poutord has worked with a variety of web technologies. He is currently focused on front-end development.
  On his day to day job, he is working as a senior front-end engineer at VSware. He is also an occasional tech speaker and a mentor.
  As a digital nomad, he is living where the WiFi and sun are 😎 <br>
  Do you want to know more? <a href="https://www.maxpou.fr/about" rel="noopener" target="_blank">Visit my website!</a>
  `,
  siteUrl: "https://maxpou.github.io/",
  disqusSiteUrl: "https://www.maxpou.fr/",
  // Prefixes all links. For cases when deployed to maxpou.fr/gatsby-starter-morning-dew/
  pathPrefix: "/froggit.fr", // Note: it must *not* have a trailing slash.
  siteCover: "cover-baymax.jpeg", // file in content/images
  googleAnalyticsId: "UA-67868977-2",
  background_color: "#ffffff",
  theme_color: "#222222",
  display: "standalone",
  icon: "content/images/baymax.png",
  postsPerPage: 4,
  disqusShortname: "maxpou",
  headerTitle: "Froggit",
  headerLinksIcon: "logo_froggit.png", //  (leave empty to disable: '')
  headerLinks: [
    {
      label: "Accueil",
      url: "/",
    },
    {
      label: "Fonctionnalités",
      url: "/",
    },
    {
      label: "FAQ",
      url: "/faq",
    },
    {
      label: "Tarifs",
      url: "/tarifs",
    },
    {
      label: "Blog",
      url: "/pages/2/",
    },
    {
      label: "Login",
      url: "/login",
    },
    {
      label: "S'enregistrer",
      url: "/enregistrement",
    },
  ],
  // Footer information (ex: Github, Netlify...)
  websiteHost: {
    name: "GitLab",
    url: "https://gitlab.com",
  },
  footerLinks: [
    {
      sectionName: "Pourquoi Froggit ?",
      links: [
        {
          label: "Ton code source est-il en sécurité ?",
          url: "https://lydra.fr/es-2-ton-code-source-est-il-vraiment-en-securite/",
        },
        {
          label: "La souveraineté numérique",
          url: "https://lydra.fr/category/souverainete-numerique/",
        },
      ],
    },
    {
      sectionName: "Communauté",
      links: [
        {
          label: "Forum",
          url: "/forum",
        },
        {
          label: "Wiki",
          url: "/wiki",
        },
      ],
    },
    {
      sectionName: "Société",
      links: [
        {
          label: "À propos",
          url: "/a_propos",
        },
        {
          label: "Contact",
          url: "/contact",
        },
        
        {
          label: "Équipe",
          url: "/equipe",
        },
        {
          label: "Mentions légales",
          url: "/mentions_legales",
        },
        {
          label: "CGV",
          url: "https://oxalis-scop.fr/conditions-generales-de-vente",
        },
        {
          label: "CPV",
          url: "/cpv",
        },
        {
          label: "CGU",
          url: "/cgu",
        },
      ],
    },
  ],
}
