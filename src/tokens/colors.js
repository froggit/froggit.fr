// 50 shades of grey generator
// https://javisperez.github.io/tailwindcolorshades/#/?%233E4047=3E4047&tv=1
const colors = {
    grey100: '#ECECED',
    grey200: '#CFCFD1',
    grey300: '#B2B3B5',
    grey400: '#78797E',
    grey500: '#3E4047',
    grey600: '#383A40',
    grey700: '#25262B',
    grey800: '#1C1D20',
    grey900: '#131315',

    white: '#ffffff',
    yellow: '#ffdc4e',
    lightYellow: '#f9e892',
    lightBlue: '#697980',
    green: '#577018',
    lightGreen: '#8EA34E',
    orange: '#E07931',
    lightOrange: '',
}

export default {
    ...colors,
    //textLightest: colors.white,
    textLightest: colors.green,
    textLightestHover: colors.lightGreen,
    //textLightestHover: colors.grey200,
    textLight: '#57595d',
    //primary: colors.grey500,
    primary: 'white',
    secondary: 'black',
    //primaryAlpha: 'rgba(32, 35, 42, 0.85)', // value per default
    primaryAlpha: 'rgba(32, 35, 42, 0.00)', // transparent color, use last value to change the variable
    primaryAlpha: 'white', 
    text: colors.grey500,
    background: 'white',
    backgroundArticle: colors.white,
    heartFooter: colors.orange,
    //links: colors.yellow,
    links: colors.orange,
    backgroundSelection: colors.yellow,
    highlight_code_oneline: '#fff9d9',
    highlight_code_bg: colors.yellow,"parser": "babel-eslint",
    highlight_code_marker: colors.yellow,
    highlight_code_linebg: '#022a4b',
    socialMediaCardFilter: '#437abf', // #8f43bf
    postMetadata: colors.lightBlue,
    // testing
    // primary: `#6556B3`,
    // primaryAlpha: `#6556B3cc`,
}
