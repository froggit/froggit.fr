import React from "react"
import { graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import Wrapper from "../components/Wrapper"
import SEO from "../components/SEO"
import RelatedPosts from "../components/RelatedPosts"
import { Text } from "../components/Commons"
import { Container, TitleTagline, TitleIcon, ContainerIcon } from "../components"
import { iconWrapper, TitleTaglineElements } from "../elements"
import { IconSecurite } from "../components"
import { IconFreedom } from "../components"
import { IconGdpr } from "../components"
import { media, colors } from "../tokens"

const IndexPage = () => {
const data = useStaticQuery(graphql`
  query {
    posts: allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        fileAbsolutePath: { regex: "//content/posts//" }
        frontmatter: { published: { ne: false }, unlisted: { ne: true } }
      }
      limit: 4
    ) {
      edges {
        node {
          excerpt
          frontmatter {
            date(formatString: "DD MMMM, YYYY")
            title
            tags
            language
            slug
          }
        }
      }
    }
  }
`)

    
const posts = data.posts.edges
  return (
    <Layout>
      <Container>
        <TitleTagline>
          Froggit aide les Dev, les Ops et les entreprises du numérique 
          à protéger leurs codes sources de l’espionnage industriel
        </TitleTagline>
      </Container>

      <ContainerIcon>
      <TitleIcon>
        
        <div>
          <IconSecurite />
          <h2>Sécurité</h2>
          <p>Vos données sont en lieu sûr et sauvegardées toutes les nuits</p>
        </div>
        </TitleIcon>

        <TitleIcon>
        <div>
          <IconFreedom />
          <h2>Souverain</h2>
          <p>Nous sommes hébergés en France et conformes au RGPD</p>
        </div>
      </TitleIcon>

       
        <TitleIcon>
        <div >
          <IconGdpr />
          <h2>Libre</h2>
          <p>Notre solution s'appuie sur des logiciels libres et nos conditions générales sont éthiques.</p>
        </div>
      
      </TitleIcon>
      </ContainerIcon>
    
      <Wrapper>
        <RelatedPosts posts={posts} />
      </Wrapper>
    </Layout>
  )
}

export default IndexPage
