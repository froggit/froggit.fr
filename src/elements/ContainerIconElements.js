import styled from "styled-components"
import { colors, media } from "../tokens";


export const ContainerIconWrapper = styled.div`
display: grid;
grid-template-columns: repeat(3, 1fr);
grid-gap: 10px;
grid-auto-rows: minmax(100px, auto);
line-height: 2;
text-align: center;
font-size: 1rem;
background-color: ${colors.green};
color: ${colors.primary};
padding: 30px;
`
;
