import styled from "styled-components"
import { colors, media } from "../tokens";

export const TitleTaglineWrapper = styled.h1`
  font-size: 4rem;
  text-align: center;
  color: ${colors.secondary};
`
