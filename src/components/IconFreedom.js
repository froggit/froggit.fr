import React from "react"
import Img from "gatsby-image"
import { useStaticQuery ,graphql } from "gatsby"
import { IconFreedomWrapper, TitleTaglineIconWrapper, TextTaglineIconWrapper } from "../elements"

export const IconFreedom = ({ fluid }) => {

    const data = useStaticQuery(graphql`
        query {
            imageSharp (fluid : {originalName: {eq: "freedom.png"}}) {
                fluid {
                    ...GatsbyImageSharpFluid
                }
            }
        }

`)
    return (
        <IconFreedomWrapper>
            <Img fluid={fluid ? fluid : data.imageSharp.fluid} style={{
                margin: "auto auto auto auto",
                width: "64px",
            }}
            />
        </IconFreedomWrapper>

       
    )

}
