import React from "react"
import Img from "gatsby-image"
import { useStaticQuery ,graphql } from "gatsby"
import { IconGdprWrapper } from "../elements"

export const IconGdpr = ({ fluid }) => {

    const data = useStaticQuery(graphql`
        query {
            imageSharp (fluid : {originalName: {eq: "gdpr.png"}}) {
                fluid {
                    ...GatsbyImageSharpFluid
                }
            }
        }

`)

    return (
        <IconGdprWrapper>
            <Img fluid={fluid ? fluid : data.imageSharp.fluid} style={{
                margin: "auto auto auto auto",
                width: "64px",
            }}
            />
        </IconGdprWrapper>
    )
}


