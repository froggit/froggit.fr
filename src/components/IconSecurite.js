import React from "react"
import Img from "gatsby-image"
import { useStaticQuery ,graphql } from "gatsby"
import { IconSecuriteWrapper } from "../elements"

export const IconSecurite = ({ fluid }) => {

    const data = useStaticQuery(graphql`
        query {
            imageSharp (fluid : {originalName: {eq: "securite.png"}}) {
                fluid {
                    ...GatsbyImageSharpFluid
                }
            }
        }

`)

    return (
        <div>
        <IconSecuriteWrapper>
            <Img fluid={fluid ? fluid : data.imageSharp.fluid} style={{
                margin: "auto auto auto auto",
                width: "64px",
            }}
            />
        </IconSecuriteWrapper>
        </div>
    )
}


