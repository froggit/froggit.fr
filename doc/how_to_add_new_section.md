# How to add new section ?

## First Step: create a component

To add a section, we need to create a component.

### Create an element

Before create a component, create a file elements where we will define the style CSS with the `styled-components` library.
We will create the file `src/elements/SectionElements.js` for this exemple.

In this file, we import the `styled-components` library like this:

```
import Styled from "styled-components"
```

Next we define the CSS properties like this:

```
const Section = styled.div`
  line-height: 1.5;
  text-align: center;
  font-size: 3rem;
`
```

### Export the element

In the file `src/elements/index.js` export the `SectionElements` file we just have create like this:

```
export * from './SectionElements';
```

### Create the component

We have now to create the file under `src/componements/Section.js`.

We will import React and our element file with a Wrapper name like this:

```
import React from "react"
import { SectionWrapper } from "../elements"
```

Next, export the const variable `Section`, that is the name of the component and return `react children` component into `SectionWrapper`.

Export by default the component.

```
export const Section = ({children}) => {
    return <SectionWrapper>{children}</SectionWrapper>
}

export default Section

```

### Export the element

In the file `src/components/index.js` export the `section` component file we just have create like this:

```
export * from './Section'
```

## Second Step: use a component

We need to import this component into the page we want to render.

For the exemple, we will called our component `"Section"`.

We have to import the component into a page we can create or that's already existing.

Into this Gatsby site, our home page's is `src/pages/index.js`, but we can create a section anywhere we want.

Once we have make this import in our page, we need to return our component in the page structure.

By exemple, if we want to include our section into the header and the footer, we have to return our section component into the layout like this:

1. Import `react`, `layout` and `section` components we want to render, that is called `Section`, and specifie where they're come from.

2. Define where we want our imports will return. Here, in Index Page (`src/pages/index.js`) with a variables defined in a const.

3. Return the `<div>` (but it could be a `<h1>`, a `<span>` or any HTML element) in our section component and include this into the `Layout`.
The `Layout` is a component that include header, footer, navbar... that is render on all pages we want.
Don't forget to export the page at the end of the code with an `export default` of the name's page.

4. Look at this exemple code in `src/pages/index.js` to create a section with a component called `Section` and that will render a `div` element.

```
import React from "react"
import Layout from "../components/layout"
import { Section } from "../components"

const IndexPage = () => {

    return (
      <Layout>
        <Section>

            <div>

        Here, you can write the content of the section, inclued links, images or any data by a query GraphQL, or passed other components... 

            </div>

        </Section>
      </Layout>
    )
}

export default IndexPage

```


## Links 

- [Learn how to create components](https://www.gatsbyjs.org/tutorial/part-one/#building-with-components)
- [Creating Nested Layout Components](https://www.gatsbyjs.org/tutorial/part-three/)
- [Why and how use Styled Components in Gatsby.js ?](https://www.gatsbyjs.org/docs/styled-components/)
- [Official site of Styled Componets](https://styled-components.com/)
