# Work with Git

[Git](https://git-scm.com) is used as a [DVCS](https://en.wikipedia.org/wiki/Distributed_version_control). In this chapter, you will find how we use Git on a daily-basis to code and collaborate with the team.

If you are looking for a good introduction to Git, take a look at the [_Git In Practice_](https://github.com/GitInPractice/GitInPractice#readme) book from Mike McQuaid.

## Git conventions

### Commit granularity

We tend to favor low granularity-but-consistent commits to a long series of commit. If you have multiple commits in a feature branch, it means that you had to address multiple issues to achieve your feature. We can run the test suite on every commit and it should always stay green 😎.

### Commit message

We follow an emoji-driven commit message format adapted from [Angular guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines). A typical commit message should look like:

```
<type>(<scope>) <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Where **type** must be an emoji chosen from the [gitmoji guide](https://gitmoji.carloscuesta.me/).

And the **scope** should point to the gatsby application or stack component that may be affected, _e.g._:

* index
* apps:core
* plugins:foo

The **subject** contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot \(.\) at the end

Just as in the subject, use the imperative, present tense for the **body**: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with the previous behavior.

The **footer** should contain any information about breaking changes and is also the place to reference GitHub/GitLab issues that this commit closes.

Breaking changes should start with the word `BREAKING CHANGE`: with a space or two newlines. The rest of the commit message is then used for this.

## Git workflows

For the sake of simplicity, to ease interaction with the community, we use the [GitHub flow](https://guides.github.com/introduction/flow/index.html) for open-source projects. In a few words:

* the `master` branch is always stable and deployable,
* tags from the master branch are considered as releases,
* contributors have to fork or create a new feature-branch to work on \(if they are allowed to in the original repository\) and propose a merge request to merge their branch to `master`.

## Working with forges

### Declaring issues

_For now, new issues are declared in a dedicated Trello board. This is a temporary situation. In the following, we will describe how it should be_ 🤓

When declaring a new issue, please describe as much as possible the **purpose** of your issue, and eventually make a **proposal** on how it should be solved or investigated. Choose wisely a label for this issue and please do not assign someone to it \(unless you already discussed with her verbally and had an agreement\).

### Working with Merge Requests \(MR\)

We recommend to create a merge request \(MR\) in GitLab as soon as possible with the `WIP` flag preceding your PR title, _e.g._ `WIP: 😎(docker) add mongo service`.

When your work is done on this PR, remove the `WIP` flag and please ensure that:

* your feature or fix is **tested ** \(all continuous integration tests should be green and code coverage **should not** decrease\),
* your feature is **documented**,
* your branch is **up-to-date** with the target branch \(it should be rebased and force-pushed\).

Once all of those requirements are met, ask for a review of your code by assigning maintainers to your PR. Your changes should be approved by **at least one contributor of the core team** to be merged.

Last but not least, a code review should not take more than half an hour per PR to be profitable for everyone. It means that you have anticipated the amount of changes required to achieve your work. If those changes are bigger than 500 lines, then you may consider to split your feature in multiple MRs.

Note that the target branch \(`master`\) will be write-protected, _i.e._ no one is allowed to push to it. Hence you will need to use the forge UI to merge your PR once all tests are green and your changes have been approved. Our PR merging strategy is: **rebase and merge** ; we do not want a merge commit.

#### Rebase your code before the review

Before code review update your branch with the new features.

```bash
git checkout master
git pull origin master
git checkout my_branch
git rebase master // resolve conflicts
git push -f origin my_branch
```
